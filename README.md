## Viaf Group Ntriples

A flink job to sort and filter viaf n-triples files.

### Deployment

1. In `sb-ust1:/swissbib_index/apps` do `git clone https://gitlab.com/swissbib/linked/viaf-grouped-ntriples.git`
2. `cd viaf-grouped-ntriples`
3. `./deploy.sh`

### Run

In `/swissbib_index/apps/flink`

`./bin/flink run /swissbib_index/apps/viaf-grouped-ntriples-assembly-1.0.0.jar --conf /swissbib_index/nas/viaf/sort/app.properties &`