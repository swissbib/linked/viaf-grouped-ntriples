#!/usr/bin/env bash

sbt clean assembly
rm /swissbib_index/apps/viaf-grouped-ntriples-assembly-*.jar
cp target/scala-2.12/viaf-grouped-ntriples-assembly-*.jar /swissbib_index/apps